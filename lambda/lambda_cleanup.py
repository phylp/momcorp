#the following code is meant to act as a proof of concept for Lambda integration with an RDS instance
#todo: the SSM parameter store client needs to be replaced with the secrets manager because that is the credential store used by Lenovo CloudDeploy
import json
import pymysql.cursors
import boto3
from pypika import MySQLQuery, Table, Field
from botocore.exceptions import ClientError

#constants for parameters, not hardcoded credentials
REGION_NAME = "us-west-2"
CREDENTIAL_SECRET_NAME = "momcorp-db-secret"
ENDPOINT_SECRET_NAME = "momcorp-db-endpoint"

session = boto3.session.Session()
client = session.client(
    service_name='secretsmanager',
    region_name=REGION_NAME
)

try:
    credentials_response = client.get_secret_value(SecretId=CREDENTIAL_SECRET_NAME)
    endpoint_response = client.get_secret_value(SecretId=ENDPOINT_SECRET_NAME)
except ClientError as e:
    raise e
else:
    if 'SecretString' in credentials_response and 'SecretString' in endpoint_response:
        secrets1 = json.loads(credentials_response['SecretString'])
        secrets2 = json.loads(endpoint_response['SecretString'])
        rds_user = secrets1['username']
        rds_password = secrets1['password']
        host = secrets2['endpoint']
    else:
        print("missing data from secrets manager")
        exit(1)

connection  = pymysql.connect(
    host = host,
    port = 3306,
    user = rds_user,
    password = rds_password,
    db = 'momcorp_db',
    charset = 'utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)

def handler(event, context):
    try:
        with connection.cursor() as cursor:
            customers = Table('customers')
            pikaQuery = MySQLQuery.from_(customers).select('first_name','last_name').where(customers.customer_id == "886313e1-3b8a-5372-9b90-0c9aee199e5d")
            sql = str(pikaQuery)
            cursor.execute(sql)
            result = cursor.fetchone()
            print(result)
    finally:
        connection.close()