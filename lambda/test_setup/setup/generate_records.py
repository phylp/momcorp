import boto3
import base64
import json
import time
import uuid
import random
import os
import shutil
import pymysql.cursors
from  datetime import datetime, timedelta
from pypika import MySQLQuery, Table, Field
from botocore.exceptions import ClientError

CREDENTIAL_SECRET_NAME = "momcorp-db-secret"
ENDPOINT_SECRET_NAME = "momcorp-db-endpoint"
REGION_NAME = "us-west-2"
BUCKET_NAME = "momcorp-orders-bucket"
FILES_PATH = os.path.join(os.getcwd(), 'items_path')

session = boto3.session.Session()
client = session.client(
    service_name='secretsmanager',
    region_name=REGION_NAME
)

def generate_random_delta():
    deltas = [0, 7, 14, 21, 30, 60, 90]
    idx = random.randrange(0, len(deltas), 1)
    return deltas[idx]

def generate_random_item():
    items = ["plush giraffe", "tiger king figurine", "shake weight", "teletubby", "casio watch", "kettle bell"]
    idx = random.randrange(0, len(items), 1)
    return items[idx]

def generate_random_order():
    addresses = ["123 Wayward Way", "34 Clementime Circle", "2231 Locus Lane"]
    ids = ["886313e1-3b8a-5372-9b90-0c9aee199e5d", "987313e1-3b8a-5372-9b91-0c9aee199e7f", "345313e1-3b8a-5372-9b90-0c9aee199e6p"]
    idx = random.randrange(0, 3, 1)
    delta = generate_random_delta()
    d = datetime.today() - timedelta(days=delta)
    tuple_id = uuid.uuid4()
    tuple_date = d.strftime('%Y-%m-%d %H:%M:%S')
    tuple_fulfilled = round(random.random())
    tuple_customer_id = ids[idx]
    tuple_item = generate_random_item()
    tuple_address = addresses[idx]
    return (str(tuple_id), tuple_date, tuple_fulfilled, tuple_customer_id, tuple_item, tuple_address)

def upload_orders_to_S3():
    s3 = boto3.resource('s3')
    print("UPLOADING THE FOLLOWING FILES TO S3 BUCKET {}...".format(BUCKET_NAME))
    for fileName in os.listdir(FILES_PATH):
        print("FILE: " + fileName)
        s3.Bucket(BUCKET_NAME).upload_file(os.path.join(FILES_PATH, fileName), fileName)
    try:
        shutil.rmtree(FILES_PATH)
    except OSError as e:
        print("Error: %s : %s" % (FILES_PATH, e.strerror))


def create_order_object(order):
    if not os.path.isdir(FILES_PATH):
        os.mkdir(FILES_PATH)
    fileName = "{}.txt".format(order[0])
    fileObj = open(os.path.join(FILES_PATH, fileName), 'w')
    fileObj.write(order[4])
    fileObj.close()

try:
    credentials_response = client.get_secret_value(SecretId=CREDENTIAL_SECRET_NAME)
    endpoint_response = client.get_secret_value(SecretId=ENDPOINT_SECRET_NAME)
except ClientError as e:
    raise e
else:
    if 'SecretString' in credentials_response and 'SecretString' in endpoint_response:
        secrets1 = json.loads(credentials_response['SecretString'])
        secrets2 = json.loads(endpoint_response['SecretString'])
        rds_user = secrets1['username']
        rds_password = secrets1['password']
        host = secrets2['endpoint']
    else:
        print("missing data from secrets manager")
        exit(1)

connection  = pymysql.connect(
    host = host,
    port = 3306,
    user = rds_user,
    password = rds_password,
    db = 'momcorp_db',
    charset = 'utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)


with connection.cursor() as cursor:
    for i in range(25):
        orders = Table('orders')
        record = generate_random_order()
        print(record)
        pika_query = MySQLQuery.into(orders).insert(record[0], record[1], record[2], record[3], record[4], record[5])
        sql = str(pika_query)
        cursor.execute(sql)
        create_order_object(record)
    connection.commit()
    result = cursor.fetchall()

print("DB records insertion complete, uploading files...")
upload_orders_to_S3()
