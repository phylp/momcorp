CREATE DATABASE momcorp_db;
USE momcorp_db;

CREATE TABLE customers (
	customer_id VARCHAR(36) NOT NULL PRIMARY KEY,
    first_name VARCHAR(100),
    last_name VARCHAR(100)
); 

CREATE TABLE orders (
	id VARCHAR(36) NOT NULL PRIMARY KEY, 
    orderdate DATETIME, 
    fulfilled BOOLEAN,
    customer_id INT,
    item VARCHAR(100)
    address VARCHAR(2000),
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

INSERT INTO customers (customer_id, first_name, last_name)
VALUES
    ('886313e1-3b8a-5372-9b90-0c9aee199e5d', 'Cypress', 'Hill'),
    ('987313e1-3b8a-5372-9b91-0c9aee199e7f', 'Stephen', 'Colbert'),
    ('345313e1-3b8a-5372-9b90-0c9aee199e6p', 'Lisa', 'Leslie');
    