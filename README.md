# MomCorp POC Instructions

## 1. Create S3 Bucket to Hold Lambda Functions

Use the AWS Console, CLI, SDK, or even another Cloudformation template to create a bucket. The sole purpose of this bucket is to hold Lambda functions. Technically this _could_ have implemented as a part of the POC CloudFormation template itself, but in the real world the number of Lambda functions required to drive microservices will grow over time, so it is not appropriate to collocate Lambda function creation and bucket creation in the same template. 

## 2. Execute `deploy.sh` with bucket name and stack name arguments
provide 2 arguments for the script:
1. name of the bucket which holds your Lambda functions
2. the name of your cloudformation stack
Example: `./deploy.sh mylambdafunctions mycfstack`

----

### Notes on script:

When deploying a Lambda function via CloudFormation, the code must be sourced from an S3 bucket. Additionally, the source must be encapsulated as a single S3 object via a `.zip` file. 

Instead of manually performing these tasks, the shell script implements the `package` command via the AWS CloudFormation CLI. The CLI automatically performs the following steps against your base template:

1. Package the hardcoded path in your template
2. Upload the packaged files to a specified S3 bucket
3. Create a new template which replaces the hardcoded `code` path property with the target S3 bucket and object key. This new template is the one which will be uploaded to CloudFront.
4. Here is the usage pattern for the command: `aws cloudformation package --s3-bucket [bucketname] --template-file [templatepath] --output-template-file [outputfilepath] --use-json`

