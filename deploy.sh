#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ];
    then echo "You must supply both a bucket name and stack name (ex: './deploy.sh mylambdabucket mylambdastack')"
    exit 1
else
    #Create and initialize python virtual environment
    echo "Creating virtual environment in python - venv"
    cd ./lambda
    virtualenv venv

    echo "Sourcing virtual environment - venv"
    source venv/bin/activate

    #Create directory to place things
    echo "Creating 'setup' directory"
    mkdir setup

    #Move the relevant files into setup directory
    echo "Moving function file(s) to setup dir"
    cp lambda_cleanup.py setup/
    cd ./setup

    # Install requirements 
    echo "pip installing requirements from requirements file in target directory"
    pip install -r ../requirements.txt -t .

    #run CloudFormation S3 package upload
    echo "packaging lambda code and uploading to S3"
    aws cloudformation package --s3-bucket "$1" --template-file ../../cloudformation_templates/base_lambda_template.json --output-template-file ../../cloudformation_templates/final_template.json --use-json
    
    #deploying Lambda function
    echo "deploying lambda to AWS"
    aws cloudformation deploy --template-file ../../cloudformation_templates/final_template.json --stack-name "$2" --capabilities CAPABILITY_IAM
    
    # Remove the setup directory used
    echo "Removing setup directory and virtual environment"
    cd ..
    rm -r ./setup
    deactivate
    rm -r venv
fi